# Auth from Scratch JS Express

Es wurden folgende Packages verwendet:

1.  Express als Backend*Server
2. Joi zur Validierung
3. Monk als Schnittstelle zu MongoDB
4. JWT (Jason Web Token) zur Best�tigung des Login
5. Volleyball zur �berwachung des Verkehrs
6. Cors als Sicherheit f�rs Fetchen zwischen Frontend und Backend
7. Vue.js als Frontend