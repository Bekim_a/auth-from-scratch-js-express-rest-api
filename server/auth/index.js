const express = require("express");
const Joi = require("joi");
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const router = express.Router();

//Mongo DB Connection
const db = require("../db/connection.js");
const users = db.get("users");
users.createIndex("username", { unique: true });

// Validation Shema
const schema = Joi.object().keys({
  username: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),
  password: Joi.string()
    .min(5)
    .required()
});

function createTokenSendResponse(user, res, next) {
  const payload = {
    _id: user._id,
    username: user.username,
  }
  jwt.sign(payload, process.env.TOKEN_SECRET, {
    expiresIn: '1d'
  }, (err, token) => {
    if(err) {
      errorCode(res, next);
    } else {
      res.json({ token});
    }
  });
}

// any route in here is pre-pended with /auth

router.get("/", (req, res, next) => {
  
});

router.post('/signup', (req, res, next) => {
    const result = Joi.validate(req.body, schema);
    if(result.error === null) {
      users.findOne({
        username: req.body.username
      }).then(user => {
        if(user) {
          const err = new Error('Der Username ist schon vergeben. Bitte nimm einen anderen!');
          res.status(409);
          next(err);
        } else {
          bcrypt.hash(req.body.password, 12).then(hashedPassword => {
            const newUser = {
              username: req.body.username,
              password: hashedPassword
            };
  
            users.insert(newUser).then(insertedUser => {
              createTokenSendResponse(insertedUser, res, next);
            });
          });
  
        }
      });
    } else {
      res.status(422)
      next(result.error);
    }
  });

  router.post('/login', (req, res, next) => {
    const result = Joi.validate(req.body, schema);
    if (result.error === null) {
      users.findOne({
        username: req.body.username,
      }).then(user => {
        if(user) {  
          bcrypt.compare(req.body.password, user.password).then((result) => {
            if (result) {
              console.log(user);
              createTokenSendResponse(user, res, next);
              console.log(createTokenSendResponse(user,res,next));
            } else {
              errorCode(res, next);
            }
          });
        } else {
          errorCode(res, next);
        }
      });
    } else {
      errorCode(res, next);
    }
  });
  
function errorCode(res, next) {
  res.status(422);
      const error = new Error('Unable to Login.')
      next(error);
}
module.exports = router;
